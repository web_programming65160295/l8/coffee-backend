import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(@InjectRepository(Product) private productsRepository: Repository<Product>,) { }

  create(createProductDto: CreateProductDto) {
    return this.productsRepository.save(createProductDto);
  }

  findAll() {
    return this.productsRepository.find({ relations: { type: true } });
  }

  findOne(id: number) {
    return this.productsRepository.findOneOrFail({ where: { id: id }, relations: { type: true } });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const updatedProduct = await this.productsRepository.findOneByOrFail({ id: id })
    await this.productsRepository.update(id, { ...updatedProduct, ...updateProductDto })
    return this.productsRepository.findOne({ where: { id: id }, relations: { type: true } });
  }

  async remove(id: number) {
    const deleteProduct = await this.productsRepository.findOneByOrFail({ id: id })
    await this.productsRepository.remove(deleteProduct)
    return deleteProduct;
  }
}
